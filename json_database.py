"""
# Module for generating a JSON database containing image metadata.
# @author: Ebling Mis
"""

import json
import locale
import os

import datetime
import exif


class JsonDatabase:
    def __init__(self, tags_to_ignore):
        locale.setlocale(locale.LC_TIME, os.environ['LANG'])
        self.__db = {
            'groups': {
                'event': [],
                'city': [],
                'family': [],
                'species': []
            },
            'pictures': []
        }
        self.__tags_to_ignore = tags_to_ignore

    # Convert a date range represented by [datebegin; dateend] (Date objects)
    # to a string.
    @staticmethod
    def __date_range_to_brief(datebegin, dateend):
        if datebegin == dateend:
            return datebegin.strftime('%d %B %Y')
        if datebegin.year == dateend.year and datebegin.month == dateend.month:
            return datebegin.strftime('%d') + ' – ' + dateend.strftime('%d %B %Y')
        if datebegin.year == dateend.year:
            return datebegin.strftime('%d %B') + ' – ' + dateend.strftime('%d %B %Y')
        return datebegin.strftime('%d %B %Y') + ' – ' + dateend.strftime('%d %B %Y')

    @staticmethod
    def __update_group_brief(group_type, group, brief):
        if brief != '':
            group['brief'] = brief
        else:
            group['brief'] = JsonDatabase.__date_range_to_brief(
                datetime.datetime.strptime(group['created'], '%Y-%m-%d').date(),
                datetime.datetime.strptime(group['lastmodified'], '%Y-%m-%d').date()
            )

    def __add_to_group(self, group_type, title, date, brief):
        # If the group already exists, update 'brief' and return the group title.
        for el in self.__db['groups'][group_type]:
            if el['id'] == title:
                if el['lastmodified'] < date:
                    el['lastmodified'] = date
                if date < el['created']:
                    el['created'] = date
                JsonDatabase.__update_group_brief(group_type, el, brief)
                return el['id']
        # Otherwise, add a new group.
        new_group = { 'id': title }
        thumbnail = 'thumbs/' + group_type + '/' + title + '.jpg'
        if os.path.exists(thumbnail):
            new_group['uri'] = thumbnail
        new_group['created'] = date
        new_group['lastmodified'] = date
        JsonDatabase.__update_group_brief(group_type, new_group, brief)
        self.__db['groups'][group_type].append(new_group)
        return new_group['id']

    @staticmethod
    def __compute_value(prefix, string, suffix, invert=False):
        if not string.strip():
            return ''
        m = string.split('/', 1)  # split by first double-colon
        val = 0.0
        if len(m) > 1:
            val = float(m[0]) / float(m[1])
        else:
            val = float(m)
        if invert and val < 1.0:
            return prefix + '1/' + str(round(1.0 / val)) + suffix
        return prefix + str(val) + suffix

    # Add the picture identified by +rel_path+ to the database.
    # +rel_path+ should be relative to the location where the database file will
    # be generated.
    def add(self, rel_path):
        meta = exif.Exif(rel_path)
        meta.read()
        time = datetime.datetime.strptime(meta.get('Exif.Photo.DateTimeOriginal'), '%Y:%m:%d %H:%M:%S')
        date = datetime.date(time.year, time.month, time.day)
        country = meta.get('Exif.Photo.UserComment:country')
        tags = meta.get('Exif.Photo.UserComment:tags').split(';')
        if tags == ['']:
          tags = []
        tags = [item for item in tags if item not in self.__tags_to_ignore]
        tags.sort()
        family = meta.get('Exif.Photo.UserComment:family')
        kingdom = meta.get('Exif.Photo.UserComment:kingdom')
        img = {}
        img['id'] = str(len(self.__db['pictures']))
        img['uri'] = rel_path
        img['height'] = meta.get('Exif.Photo.PixelYDimension')
        img['width'] = meta.get('Exif.Photo.PixelXDimension')
        img['title'] = meta.get('Exif.Photo.UserComment:title')
        img['datetime'] = time.isoformat()
        img['artist'] = meta.get('Exif.Image.Artist')
        model = meta.get('Exif.Image.Make') + ' ' + meta.get('Exif.Image.Model')
        img['model'] = model.rstrip()
        img['exposure'] = JsonDatabase.__compute_value('', meta.get('Exif.Photo.ExposureTime'), ' s', True)
        img['fnumber'] = JsonDatabase.__compute_value('f/', meta.get('Exif.Photo.FNumber'), '')
        img['focallength'] = JsonDatabase.__compute_value('', meta.get('Exif.Photo.FocalLength'), ' mm')
        img['isospeed'] = meta.get('Exif.Photo.ISOSpeedRatings')
        img['event'] = self.__add_to_group('event', meta.get('Exif.Photo.UserComment:event'), date.isoformat(), '')
        img['tags'] = tags
        img['city'] = self.__add_to_group('city', meta.get('Exif.Photo.UserComment:city'), date.isoformat(), country)
        img['country'] = country
        if family != '':
            img['family'] = self.__add_to_group('family', family, date.isoformat(), kingdom)
        if meta.get('Exif.Photo.UserComment:species') != '':
            img['species'] = self.__add_to_group('species', meta.get('Exif.Photo.UserComment:species'),
                                                 date.isoformat(), family)
        if meta.get('Exif.Photo.UserComment:iNaturalistID') != '':
            img['observation'] = 'https://www.inaturalist.org/observations/' + meta.get(
                'Exif.Photo.UserComment:iNaturalistID')
        self.__db['pictures'].append(img)

    def to_json(self):
        return json.dumps(self.__db, ensure_ascii=False)
