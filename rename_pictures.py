#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import glob
import os
import sys

# To get py3exiv2 on Debian system where it is not available through the
# package manager, install the following packages:
#   python-all-dev
#   libexiv2-dev
#   libboost-python-dev
# Then run `pip3 install py3exiv2'.
import pyexiv2

# Parse arguments
parser = argparse.ArgumentParser()
args = parser.parse_args()

# Ask for user confirmation
print("This script will update EXIF tags and rename pictures in the following folder:")
print("  " + os.path.abspath("./"))
answer = ""
while answer not in ["y", "n"]:
    answer = input("Are you sure ? [y/n] ").lower()
if answer == "n":
    sys.exit()

i = 0
for picture in sorted(glob.glob("./*.[jJ][pP][gG]")):
    i += 1
    abs_path = os.path.abspath(picture)
    exif = pyexiv2.ImageMetadata(abs_path)
    exif.read()
    date = exif["Exif.Photo.DateTimeOriginal"].value

    # Add copyright information to file
    exif["Exif.Image.Artist"] = "Thibaud Backenstrass"
    exif["Exif.Image.Copyright"] = (
        "Copyright, Thibaud Backenstrass, "
        + date.strftime("%Y")
        + ". All rights reserved."
    )
    exif.write()

    # Rename file using EXIF date & time
    if os.path.basename(abs_path).startswith(date.strftime("%Y_%m_%d %Hh%M ")):
        print(picture + " skipped")
    else:
        new_name = os.path.join(
            os.path.dirname(abs_path),
            date.strftime("%Y_%m_%d %Hh%M %S_") + str(i) + ".jpg",
        )
        print(f"{picture} -> {os.path.basename(new_name)}")
        os.rename(abs_path, new_name)
