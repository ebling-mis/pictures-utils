"""
# Module for checking that EXIF data are properly filled.
# @author: Ebling Mis
"""

import os
import re
import requests
from exif import Exif


# Taxon hierarchy (clades):
#  - Kingdom (règne)
#  - Phylum (embranchement)
#  - Class (classe)
#  - Order (ordre)
#  - Family (famille)
#  - Tribe (tribu)
#  - Genus (genre)
#  - Section (section)
#  - Species (espèce)
# For some clades, additional classifications are used: super-* and sub-*

class ExifChecker:
    def __init__(self, rootdir):
        self.__rootdir = rootdir
        self.__current_file_path = ''
        self.__current_file_errors = 0
        self.__online = False

    # Make sure that a given EXIF property is set, ie. is not empty.
    def __ensure_isset(self, exif, property):
        if exif.is_empty(property):
            # If this is the first error we detect in this file, print file name
            if self.__current_file_errors == 0:
                print('\033[1;37m' + self.__current_file_path + '\033[0m')
            print('  * \033[93m' + property + '\033[0m should not be empty')
            self.__current_file_errors += 1
            return False
        else:
            return True

    # Make sure that a given EXIF property matches the given regexp.
    def __ensure_matches(self, exif, property, regexp):
        if not exif.match(property, regexp):
            # If this is the first error we detect in this file, print file name
            if self.__current_file_errors == 0:
                print('\033[1;37m' + self.__current_file_path + '\033[0m')
            print('  * \033[93m' + property + '\033[0m should match \'' + regexp + '\'')
            self.__current_file_errors += 1
            return False
        else:
            return True

    # Check that basic EXIF metadata are filled.
    def __check_basic_exif(self, exif, is_a_png_file, is_a_panorama_or_thumbnail):
        self.__ensure_isset(exif, 'Exif.Image.Artist')
        self.__ensure_matches(exif, 'Exif.Image.Copyright', '^Copyright.*\d{4}.*$')
        self.__ensure_matches(exif, 'Exif.Image.Orientation', '^1?$')
        self.__ensure_isset(exif, 'Exif.Photo.PixelXDimension')
        self.__ensure_isset(exif, 'Exif.Photo.PixelYDimension')
        self.__ensure_isset(exif, 'Exif.Photo.DateTimeOriginal')
        # Don't check camera model if this is a PNG file (probably not a photography)
        if not is_a_png_file:
            self.__ensure_isset(exif, 'Exif.Image.Make')
            self.__ensure_isset(exif, 'Exif.Image.Model')
        # Don't check imaging parameters if this is not a photography
        if not is_a_png_file and not is_a_panorama_or_thumbnail:
            self.__ensure_isset(exif, 'Exif.Photo.ExposureTime')
            self.__ensure_isset(exif, 'Exif.Photo.FNumber')
            self.__ensure_isset(exif, 'Exif.Photo.FocalLength')
            self.__ensure_isset(exif, 'Exif.Photo.ISOSpeedRatings')

    # Check that the Exif.Photo.UserComment property is correctly filled.
    def __check_user_comment(self, exif):
        self.__ensure_isset(exif, 'Exif.Photo.UserComment:title')
        self.__ensure_matches(exif, 'Exif.Image.UserComment:region', '^$') # TODO: temporaire
        self.__ensure_isset(exif, 'Exif.Photo.UserComment:tags')
        self.__ensure_isset(exif, 'Exif.Photo.UserComment:country')
        self.__ensure_isset(exif, 'Exif.Photo.UserComment:city')
        self.__ensure_isset(exif, 'Exif.Photo.UserComment:event')
        if not exif.is_empty('Exif.Photo.UserComment:iNaturalistID'):
            id = exif.get('Exif.Photo.UserComment:iNaturalistID')
            self.__check_inaturalist(exif, int(id))

    # Check that EXIF metadata are compatible with iNaturalistID.
    def __check_inaturalist(self, exif, id):
        if self.__online:
            r = requests.get('http://api.inaturalist.org/v1/observations/' + str(id))
            if r.status_code == 200:
                # iNaturalist REST API reference: https://api.inaturalist.org/v1/docs/
                json = r.json()
                taxon = json['results'][0]['taxon']['name']
                rank = json['results'][0]['taxon']['rank']
                m = taxon.split(' ', 2)  # split by space, at most twice
                if len(m) > 2:
                    taxon = m[0] + ' ' + m[1] + ', ' + m[2]  # add comma betw. species and subspecies
                # FIXME: for now, we only check ID at species/subspecies levels
                if rank == 'species' or rank == 'subspecies':
                    if not self.__ensure_matches(exif, 'Exif.Photo.UserComment:species', '\(' + taxon + '\)'):
                        print('    See https://www.inaturalist.org/observations/' + str(id) +
                              ' (rank: ' + json['results'][0]['taxon']['rank'] + ')')
            else:
                print('Cannot check ID ' + str(id) + ' (request returned ' + r.status_code + ')')

    # Check EXIF metadata from all images files located in the rootdir. Return the
    # number of errors detected. If online, additional checks are performed on some
    # pictures.
    def check(self, online):
        self.__online = online
        total_errors = 0
        files_in_error = 0

        # Identify all image files in the current directory, recursively
        for root, dirs, files in os.walk(self.__rootdir):
            dirs[:] = [d for d in dirs if not d[0] == '.']  # ignore hidden directories
            dirs[:] = [d for d in dirs if not d[0] == '.web']  # ignore .web directories
            dirs.sort()
            for file in sorted(files):
                rel_path = os.path.join(root, file)
                abs_path = os.path.abspath(rel_path)
                filename = os.path.basename(rel_path)
                is_a_png_file = re.search('\.png$', rel_path)
                is_a_panorama = re.search('\(panos?\d+.*\)', abs_path)
                is_a_thumbnail = rel_path.startswith('./thumbs/')
                exif = Exif(abs_path)
                try:
                    exif.read()
                except (TypeError, IOError):
                    # print('Skipping non-image file: ' + rel_path)
                    pass
                else:
                    self.__current_file_path = rel_path
                    self.__current_file_errors = 0

                    self.__check_basic_exif(exif, is_a_png_file, is_a_panorama or is_a_thumbnail)
                    if not is_a_thumbnail:
                        self.__check_user_comment(exif)

                    if self.__current_file_errors > 0:
                        total_errors += self.__current_file_errors
                        files_in_error += 1

        return total_errors, files_in_error
