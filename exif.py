"""
# Module for accessing and modifying EXIF metadata from a picture file.
# @see: https://www.exiv2.org/metadata.html
# @author: Ebling Mis
"""

import json
import os
import re

# To get py3exiv2 on Debian system where it is not available through the
# package manager, install the following packages:
#   python-all-dev
#   python3-pip
#   libexiv2-dev
#   libboost-python-dev
# Then run `pip3 install py3exiv2'.
import pyexiv2


class Exif:
    def __init__(self, path):
        self.__path = path
        self.__exif = pyexiv2.ImageMetadata(path)
        self.__modified = False

    # Read EXIF metadata from file.
    def read(self):
        self.__exif.read()
        self.__modified = False

    # Converts a JSON string to a hash
    @staticmethod
    def __json_to_hash(string):
        try:
            return json.loads(string)
        except ValueError:
            return {}

    # Get the value of an EXIF metadata by its key (selector). Returns an empty
    # string if the key does not exist.
    # If the key contains the ':' character, the EXIF metadata is parsed as a JSON
    # hash, the first part of the selector being the EXIF key and the second part
    # the JSON key.
    def get(self, selector):
        m = selector.split(':', 1)  # split by first double-colon
        try:
            exif_data = self.__exif[m[0]].raw_value
        except KeyError:
            exif_data = ''
        # If present, drop character encoding at the beginning of UserComment
        if m[0] == 'Exif.Photo.UserComment' and exif_data.startswith('charset='):
            exif_data = exif_data.split(' ', maxsplit=1)[1]

        if len(m) > 1:
            try:
                exif_data = Exif.__json_to_hash(exif_data)[m[1]]
            except KeyError:
                exif_data = ''
        return exif_data

    # Change the value of an EXIF metadata. See @get.
    def set(self, selector, new_value):
        m = selector.split(':', 1)  # split by double-colon, at most once
        try:
            raw_exif = self.__exif[m[0]].raw_value
        except KeyError:
            raw_exif = ''
        # If present, drop character encoding at the beginning of UserComment
        if m[0] == 'Exif.Photo.UserComment' and raw_exif.startswith('charset='):
            raw_exif = raw_exif.split(' ', maxsplit=1)[1]

        new_exif = new_value
        if len(m) > 1:
            h = Exif.__json_to_hash(raw_exif)
            if new_value != '':
                h[m[1]] = new_value
            elif m[1] in h:
                del h[m[1]]
            new_exif = json.dumps(h, ensure_ascii=False)

        # Write character encoding at the beginning of UserComment
        if m[0] == 'Exif.Photo.UserComment':
            new_exif = 'charset=Unicode ' + new_exif

        # Only write the new EXIF metadata if changed
        if raw_exif != new_exif:
            print(m[0] + ' = ' + new_exif)
            # FIXME: this is ugly, we should detect the type and convert when appropriate
            if selector == 'Exif.Photo.PixelXDimension' or selector == 'Exif.Photo.PixelYDimension':
                self.__exif[m[0]] = int(new_exif)
                self.__exif[m[0]].value = int(new_exif)
            else:
                self.__exif[m[0]] = new_exif
                self.__exif[m[0]].raw_value = new_exif
            self.__modified = True

    # Write all modified EXIF metadata to the file.
    def write(self):
        if self.__modified:
            os.chmod(self.__path, 0o640)
            self.__exif.write()
            os.chmod(self.__path, 0o440)
            self.read()

    # Test if an EXIF property is empty or undefined.
    def is_empty(self, selector):
        exif_value = self.get(selector)
        if exif_value.strip():
            return False
        else:
            return True

    # Make sure an EXIF property value matches a given regex.
    def match(self, selector, pattern):
        exif_value = self.get(selector)
        if re.search(pattern, exif_value):
            return True
        else:
            return False
