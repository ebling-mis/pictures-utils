"""
# Module for manipulating EXIF metadata through a CSV file.
# @author: Ebling Mis
"""

import csv
import os
from exif import Exif


class ExifCsvEditor:
    def __init__(self, rootdir, csv_filename):
        self.__rootdir = rootdir
        self.__csv_filename = csv_filename

    # Extract EXIF metadata from all images files located in the rootdir to the
    # CSV file. Only specified EXIF properties are read. Hidden directories are
    # ignored. For each property read, the substitute_exif_value() callback is
    # executed, allowing to alter the property value before writing to CSV.
    def extract(self, exif_properties, substitute_exif_value):
        with open(self.__csv_filename, 'w') as csvfile:
            f = csv.writer(csvfile)

            # Write CSV headers
            f.writerow(['File'] + exif_properties)

            # Identify all image files in the current directory, recursively
            for root, dirs, files in os.walk(self.__rootdir):
                dirs[:] = [d for d in dirs if not d[0] == '.']  # ignore hidden directories
                dirs.sort()
                for file in sorted(files):
                    rel_path = os.path.join(root, file)
                    abs_path = os.path.abspath(rel_path)
                    exif = Exif(abs_path)
                    try:
                        exif.read()
                    except (TypeError, IOError):
                        print('Skipping non-image file: ' + rel_path)
                        pass
                    else:
                        # Write data to CSV
                        data = []
                        for selector in exif_properties:
                            exif_data = exif.get(selector)
                            exif_data = substitute_exif_value(abs_path, selector, exif_data)
                            data += [exif_data]
                        f.writerow([abs_path] + data)

    # Write EXIF metadata to images files from the CSV file. Only specified
    # EXIF properties are written.
    def write(self, exif_properties):
        with open(self.__csv_filename, 'r') as csvfile:
            f = csv.DictReader(csvfile)

            # Process each file in the CSV
            for row in f:
                img = row['File']
                exif = Exif(img)
                try:
                    exif.read()
                except IOError:
                    print('Skipping non-image or missing file: ' + img)
                    pass
                else:
                    for selector in exif_properties:
                        exif.set(selector, row[selector])
                    exif.write()
